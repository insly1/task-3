-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 14, 2020 at 08:51 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insly_task3`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employee_id` int NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `ssn_code` int DEFAULT NULL,
  `is_employee` tinyint(1) DEFAULT NULL,
  `contact` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `NAME`, `birthday`, `ssn_code`, `is_employee`, `contact`) VALUES
(2, 'Jenney', '2000-01-01', 123456789, 1, 'tel: 55 333 777, e@mail.com, adress');

--
-- Triggers `employees`
--
DELIMITER $$
CREATE TRIGGER `employee_delete_trigger` AFTER DELETE ON `employees` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('DELETE', CURRENT_TIMESTAMP, user(), 'employees')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_insert_trigger` AFTER INSERT ON `employees` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('INSERT', CURRENT_TIMESTAMP, user(), 'employees')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_update_trigger` AFTER UPDATE ON `employees` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('UPDATE', CURRENT_TIMESTAMP, user(), 'employees')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_audit_log`
--

CREATE TABLE `employee_audit_log` (
  `id` int NOT NULL,
  `action` varchar(50) DEFAULT NULL,
  `changedat` datetime DEFAULT NULL,
  `created_by` varchar(255) NOT NULL,
  `table_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_audit_log`
--

INSERT INTO `employee_audit_log` (`id`, `action`, `changedat`, `created_by`, `table_name`) VALUES
(1, 'INSERT', '2020-12-14 20:49:17', 'root@localhost', 'employees'),
(2, 'INSERT', '2020-12-14 20:49:17', 'root@localhost', 'employees'),
(3, 'INSERT', '2020-12-14 20:49:17', 'root@localhost', 'employee_disc'),
(4, 'INSERT', '2020-12-14 20:49:17', 'root@localhost', 'employee_disc'),
(5, 'UPDATE', '2020-12-14 20:49:55', 'root@localhost', 'employees'),
(6, 'DELETE', '2020-12-14 20:50:53', 'root@localhost', 'employee_disc'),
(7, 'DELETE', '2020-12-14 20:50:53', 'root@localhost', 'employees');

-- --------------------------------------------------------

--
-- Table structure for table `employee_disc`
--

CREATE TABLE `employee_disc` (
  `id` int NOT NULL,
  `english_intro` text,
  `spanish_intro` text,
  `french_intro` text,
  `english_work_ex` text,
  `spanish_work_ex` text,
  `french_work_ex` text,
  `english_education` text,
  `spanish_education` text,
  `french_education` text,
  `employee_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_disc`
--

INSERT INTO `employee_disc` (`id`, `english_intro`, `spanish_intro`, `french_intro`, `english_work_ex`, `spanish_work_ex`, `french_work_ex`, `english_education`, `spanish_education`, `french_education`, `employee_id`) VALUES
(2, 'English intro Jenney', 'intro español Jenney', 'intro française Jenney', 'english work experience Jenney', '\r\nexperiencia laboral española Jenney', 'expérience professionnelle française Jenney', 'english education Jenney', 'educación española Jenney', 'éducation française Jenney', 2);

--
-- Triggers `employee_disc`
--
DELIMITER $$
CREATE TRIGGER `employee_desc_delete_trigger` AFTER DELETE ON `employee_disc` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('DELETE', CURRENT_TIMESTAMP, user(), 'employee_disc')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_desc_update_trigger` AFTER UPDATE ON `employee_disc` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('UPDATE', CURRENT_TIMESTAMP, user(), 'employee_disc')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `employee_disc_insert_trigger` AFTER INSERT ON `employee_disc` FOR EACH ROW INSERT INTO employee_audit_log (action, changedat, created_by, table_name) 
VALUES ('INSERT', CURRENT_TIMESTAMP, user(), 'employee_disc')
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `employee_audit_log`
--
ALTER TABLE `employee_audit_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_disc`
--
ALTER TABLE `employee_disc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_audit_log`
--
ALTER TABLE `employee_audit_log`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `employee_disc`
--
ALTER TABLE `employee_disc`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee_disc`
--
ALTER TABLE `employee_disc`
  ADD CONSTRAINT `employee_disc_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
