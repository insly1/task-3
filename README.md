## Task 3 - sql
 - git clone https://gitlab.com/insly1/task-3
 - docker-compose up -d
 - Check localhost (on Windows http://192.168.99.100/)
 - Login -> Server:db; Username:root; Password:123
 - After login run -> docker exec -i db mysql -uroot -p123 insly_task3 < mysql-dump/insly_task3.sql
 - Table employee_audit_log is already working with some information


## Sql queries

# For adding user
 INSERT INTO employees (name, birthday, ssn_code, is_employee, contact)
 VALUES ('John', '2000-01-01', '123456789', 1, 'tel: 55 111 222, e@mail.com, adress');

 INSERT INTO employee_disc (english_intro, spanish_intro, french_intro, english_work_ex, spanish_work_ex, french_work_ex, english_education, spanish_education, french_education, employee_id)
 VALUES ('English intro John', 'intro español John', 'intro française John', 'english work experience John', '
 experiencia laboral española John', 'expérience professionnelle française John', 'english education John', 'educación española John', 'éducation française John', 3);

# For getting info of user
SELECT * FROM employees NATURAL JOIN employee_disc WHERE employees.name = 'John';

# For updating user info
UPDATE employees SET name = "Maks1m" WHERE employees.name = 'John';

# For deleting user
DELETE FROM employee_disc WHERE employee_id = 3;
DELETE FROM employees WHERE employee_id = 3;

# Audit
 - Check employee_audit_log table
